package com.katonaaron.energy.domain.useraccount

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.Validatable
import com.katonaaron.energy.domain.client.Client
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*
import javax.validation.constraints.NotBlank

@DDD.DomainEntity
@Entity
class UserAccount(
    @EmbeddedId
    @AttributeOverride(name = "value", column = Column(name = "id"))
    val id: UserAccountId,

    @Column(unique = true)
    private val username: String,

    @field:NotBlank()
    private var password: String,

    val role: UserRole
) : Validatable, UserDetails {

    @OneToOne(
        mappedBy = "account",
        optional = true
    )
    val client: Client? = null

    fun changePassword(newPassword: String) {
        this.password = newPassword
        validate(this)
    }

    init {
        validate(this)
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> =
        mutableListOf(SimpleGrantedAuthority(role.name))

    override fun getPassword(): String = password

    override fun getUsername(): String = username

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    override fun isCredentialsNonExpired(): Boolean = true

    override fun isEnabled(): Boolean = true
}
