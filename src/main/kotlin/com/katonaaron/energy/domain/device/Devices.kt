package com.katonaaron.energy.domain.device

import com.katonaaron.energy.domain.DDD

@DDD.Repository
interface Devices {
    fun nextIdentity(): DeviceId

    fun save(device: Device): Device

    fun findAll(): Collection<Device>

    fun findById(id: DeviceId): Device?

    fun delete(device: Device)

    fun existsById(id: DeviceId): Boolean
}
