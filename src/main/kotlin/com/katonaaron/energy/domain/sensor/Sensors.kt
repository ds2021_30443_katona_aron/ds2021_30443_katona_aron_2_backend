package com.katonaaron.energy.domain.sensor

import com.katonaaron.energy.domain.DDD

@DDD.Repository
interface Sensors {
    fun nextIdentity(): SensorId

    fun save(sensor: Sensor): Sensor

    fun findAll(): Collection<Sensor>

    fun findById(id: SensorId): Sensor?

    fun delete(sensor: Sensor)

    fun existsById(id: SensorId): Boolean
}
