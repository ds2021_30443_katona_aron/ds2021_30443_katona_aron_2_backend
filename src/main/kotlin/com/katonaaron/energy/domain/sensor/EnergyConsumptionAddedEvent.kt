package com.katonaaron.energy.domain.sensor

import com.katonaaron.energy.domain.device.EnergyConsumption
import org.springframework.context.ApplicationEvent

data class EnergyConsumptionAddedEvent(
    val sensorId: SensorId,
    val consumption: EnergyConsumption
) : ApplicationEvent(sensorId)
