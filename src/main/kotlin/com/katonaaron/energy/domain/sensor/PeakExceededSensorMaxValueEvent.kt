package com.katonaaron.energy.domain.sensor

import com.katonaaron.energy.domain.energy.Energy
import org.springframework.context.ApplicationEvent

class PeakExceededSensorMaxValueEvent(
    val sensorId: SensorId,
    val sensorMaxValue: Energy,
    val peak: Energy
) : ApplicationEvent(sensorId)
