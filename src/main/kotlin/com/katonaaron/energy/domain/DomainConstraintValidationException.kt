package com.katonaaron.energy.domain

/**
 * The DomainConstraintValidationException wraps all exceptions caused by rules violation in the
 * creation of an domain object such as Entity, Value Object or Aggregate
 */
class DomainConstraintValidationException(
    message: String,
    val details: Collection<*>? = null
) :
    RuntimeException(message)
