package com.katonaaron.energy.domain.client

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.Validatable
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.useraccount.UserAccount
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Past
import javax.validation.constraints.Size

@DDD.DomainEntity
@Entity
class Client(
    @EmbeddedId
    @AttributeOverride(name = "value", column = Column(name = "id"))
    val id: ClientId,
    name: String,
    birthDate: LocalDate,
    address: String,

    @OneToOne(
        fetch = FetchType.LAZY,
        orphanRemoval = true
    )
    val account: UserAccount
) : Validatable {

    @Size(min = 3, max = 255, message = "The name must be at least 3, at most 255 characters long")
    @NotBlank(message = "Must provide a name")
    var name: String = name
        set(value) {
            field = value
            validate(this)
        }

    @Past(message = "The birthday must be in the past")
    var birthDate: LocalDate = birthDate
        set(value) {
            field = value
            validate(this)
        }

    @NotBlank(message = "Must provide an address")
    var address: String = address
        set(value) {
            field = value
            validate(this)
        }

    @OneToMany(
        fetch = FetchType.LAZY,
        mappedBy = "owner"
    )
    private val _devices: MutableSet<Device> = mutableSetOf()
    val devices: Collection<Device>
        get() = _devices

    fun assignDevice(device: Device) {
        if (_devices.contains(device)) {
            return
        }
        _devices.add(device)
        device.setOwner(this)
        validate(this)
    }

    fun removeDevice(device: Device) {
        if (_devices.contains(device)) {
            _devices.remove(device)
            device.removeOwner()
            validate(this)
        }
    }

    fun hasDevice(device: Device): Boolean = _devices.contains(device)

    fun hasAnyDevice(): Boolean = _devices.isNotEmpty()

    fun removeAllDevices() = _devices.toMutableList().forEach { removeDevice(it) }

    init {
        validate(this)
    }

    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> true
            other is Client -> id == other.id
            else -> false
        }
    }

    override fun hashCode(): Int = Objects.hash(id)
}
