package com.katonaaron.energy.domain.energy

enum class EnergyUnit {
    kWh,
    J
}
