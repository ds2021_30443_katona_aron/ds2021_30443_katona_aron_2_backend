package com.katonaaron.energy.domain.energy

import com.katonaaron.energy.domain.DDD
import org.springframework.stereotype.Component
import javax.persistence.Access
import javax.persistence.AccessType
import javax.persistence.Embeddable
import javax.persistence.Transient
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero

@DDD.ValueObject
@Embeddable
@Access(AccessType.FIELD)
class Energy(amount: Double, unit: EnergyUnit) : Comparable<Energy> {
    companion object {
        val SI_UNIT = EnergyUnit.J

        private const val kWh_IN_J = 3_600_000

        fun convertToSI(amount: Double, from: EnergyUnit): Double {
            return when (from) {
                EnergyUnit.J -> amount
                EnergyUnit.kWh -> amount * kWh_IN_J
            }
        }

        fun convertFromSI(amount: Double, to: EnergyUnit): Double {
            return when (to) {
                EnergyUnit.J -> amount
                EnergyUnit.kWh -> amount / kWh_IN_J
            }
        }
    }

    private val amountSI: Double = convertToSI(amount, unit)

    constructor(amount: Long, unit: EnergyUnit) : this(amount.toDouble(), unit)

    fun amount(unit: EnergyUnit): Double = convertFromSI(amountSI, unit)

    @Transient
    fun isPositiveOrZero(): Boolean = amountSI >= 0

    @Transient
    fun isPositive(): Boolean = amountSI > 0

    override fun toString(): String = "${convertFromSI(amountSI, EnergyUnit.kWh)} kWh"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Energy) return false

        if (amountSI != other.amountSI) return false

        return true
    }

    override fun hashCode(): Int {
        return amountSI.hashCode()
    }

    override fun compareTo(other: Energy): Int {
        return amountSI.compareTo(other.amountSI)
    }

    operator fun plus(increment: Energy): Energy =
        Energy(
            amountSI + increment.amountSI,
            SI_UNIT
        )

    operator fun minus(decrement: Energy): Energy =
        Energy(
            amountSI - decrement.amountSI,
            SI_UNIT
        )

    operator fun div(number: Number): Energy =
        Energy(
            amountSI / number.toDouble(),
            SI_UNIT
        )
}

@Component
class PositiveOrZeroEnergyValidator : ConstraintValidator<PositiveOrZero, Energy> {
    override fun isValid(energy: Energy?, ctx: ConstraintValidatorContext?): Boolean {
        return energy?.amount(Energy.SI_UNIT)?.let { it >= 0 } ?: false
    }
}

@Component
class PositiveEnergyValidator : ConstraintValidator<Positive, Energy> {
    override fun isValid(energy: Energy?, ctx: ConstraintValidatorContext?): Boolean {
        return energy?.amount(Energy.SI_UNIT)?.let { it > 0 } ?: false
    }
}
