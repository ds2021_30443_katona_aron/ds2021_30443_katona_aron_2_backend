package com.katonaaron.energy.domain

import javax.validation.Validation

interface Validatable {
    fun <T> validate(obj: T) {
        val actualViolations = validator.validate(obj)
        if (actualViolations.isNotEmpty()) {
            throw DomainConstraintValidationException(actualViolations.toString()/*, actualViolations*/)
        }
    }

    companion object {
        private val factory = Validation.buildDefaultValidatorFactory()
        private val validator = factory.validator
    }
}
