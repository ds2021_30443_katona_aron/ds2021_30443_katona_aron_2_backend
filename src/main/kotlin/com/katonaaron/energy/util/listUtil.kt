package com.katonaaron.energy.util

fun <T> MutableList<T>.prepend(element: T) {
    add(0, element)
}

fun <T> MutableList<T>.append(element: T) {
    add(element)
}
