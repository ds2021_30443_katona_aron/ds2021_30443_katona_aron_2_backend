package com.katonaaron.energy.util

import java.time.*

fun Instant.toLocalDate(): LocalDate = LocalDate.ofInstant(this, ZoneId.systemDefault())

fun Instant.toZonedDateTime(): ZonedDateTime = atZone(ZoneId.systemDefault())

operator fun Instant.minus(other: Instant): Duration = Duration.between(other, this)
