package com.katonaaron.energy.exposition.controller

import com.katonaaron.energy.exposition.dto.Greeting
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Tag(name = "greetings", description = "the greetings API")
class GreetingsController {

    @GetMapping("/")
    fun greetings() = Greeting("greetings")
}
