package com.katonaaron.energy.exposition.controller

import com.katonaaron.energy.application.sensors.ViewSingleSensor
import com.katonaaron.energy.application.useraccount.GetUserAccount
import com.katonaaron.energy.domain.sensor.EnergyConsumptionAddedEvent
import com.katonaaron.energy.domain.sensor.PeakExceededSensorMaxValueEvent
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.simp.SimpMessageSendingOperations
import org.springframework.messaging.simp.annotation.SendToUser
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Controller
import java.security.Principal


@Controller
class NotificationController(
    private val messagingTemplate: SimpMessageSendingOperations,
    private val getUserAccount: GetUserAccount,
    private val viewSingleSensor: ViewSingleSensor,
) {

    @MessageMapping("/notifications")
    @SendToUser("/queue/notifications")
    fun getNotifications(
        @Payload message: String,
        principal: Principal
    ): String {
        println("Here")
        Thread.sleep(1000)
        return "ALL good"
    }

    @EventListener
    @Async
    fun onPeakExceededSensorMaxValueEvent(event: PeakExceededSensorMaxValueEvent) {
        val sensor = viewSingleSensor.viewSingleSensor(event.sensorId)
        this.getUserAccount.getAccountOfSensor(event.sensorId)?.let { user ->
            logger.debug("Send ${event.javaClass.simpleName} event to client")
            messagingTemplate.convertAndSendToUser(
                user.username,
                "/queue/notifications",
                "High energy consumption peak: ${event.peak}! Sensor: ${sensor.description}"
            )
        }
    }

    @EventListener
    @Async
    fun onEnergyConsumptionAddedEvent(event: EnergyConsumptionAddedEvent) {
        this.getUserAccount.getAccountOfSensor(event.sensorId)?.let { user ->
            logger.debug("Send ${event.javaClass.simpleName} event to client")
            messagingTemplate.convertAndSendToUser(
                user.username,
                "/queue/update-consumptions",
                event.sensorId.value
            )
        }
    }

//    @MessageExceptionHandler
//    @SendToUser("/queue/errors")
//    fun handleException(exception: Throwable): String? {
//        return exception.message
//    }

    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val logger = LoggerFactory.getLogger(javaClass.enclosingClass)
    }
}
