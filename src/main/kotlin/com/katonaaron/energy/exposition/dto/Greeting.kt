package com.katonaaron.energy.exposition.dto

data class Greeting(val message: String)
