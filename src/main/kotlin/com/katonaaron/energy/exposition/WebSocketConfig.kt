package com.katonaaron.energy.exposition

import org.springframework.context.annotation.Configuration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer


@Configuration
@EnableWebSocketMessageBroker
@EnableWebSocket
class WebSocketConfig : /*AbstractSecurityWebSocketMessageBrokerConfigurer(),*/ WebSocketMessageBrokerConfigurer {
    override fun configureMessageBroker(config: MessageBrokerRegistry) {
        config.enableSimpleBroker("/topic", "/queue")
        config.setApplicationDestinationPrefixes("/app")
    }

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry.addEndpoint("ws")
            .setAllowedOriginPatterns("*")
//            .setHandshakeHandler(object : DefaultHandshakeHandler() {
//                fun beforeHandshake(
//                    request: ServerHttpRequest,
//                    response: ServerHttpResponse?,
//                    wsHandler: WebSocketHandler?,
//                    attributes: MutableMap<String, String>
//                ): Boolean {
//                    if (request is ServletServerHttpRequest) {
//                        val session = request
//                            .servletRequest.session
//                        attributes["sessionId"] = session.id
//                    }
//                    return true
//                }
//            })
            .withSockJS()
    }


    //    @Override
//    public void configureClientInboundChannel(ChannelRegistration registration) {
//        registration.interceptors(new ChannelInterceptor() {
//            @Override
//            public Message<?> preSend(Message<?> message, MessageChannel channel) {
//            StompHeaderAccessor accessor =
//            MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
//            if (StompCommand.CONNECT.equals(accessor.getCommand())) {
//                Authentication user = ... ; // access authentication header(s)
//                accessor.setUser(user);
//            }
//            return message;
//        }
//        });
//    }

//    override fun configureInbound(
//        messages: MessageSecurityMetadataSourceRegistry
//    ) {
//        messages
//            .simpDestMatchers("/secured/**").authenticated()
//            .anyMessage().authenticated()
//    }
}
