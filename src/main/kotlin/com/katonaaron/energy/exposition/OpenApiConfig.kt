package com.katonaaron.energy.exposition

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.servers.Server
import org.springframework.context.annotation.Configuration

@Configuration
@OpenAPIDefinition(
    info = Info(
        title = "Energy Platform",
        version = "dev-0.0.1"
    ),
    servers = [
        Server(
            url = "\${energy.prod-server}",
            description = "Production server"
        )
    ]
)
class OpenApiConfig
