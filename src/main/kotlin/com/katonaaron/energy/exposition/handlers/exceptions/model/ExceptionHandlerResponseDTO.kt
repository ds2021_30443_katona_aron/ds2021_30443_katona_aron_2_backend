package com.katonaaron.energy.exposition.handlers.exceptions.model

import java.util.*

class ExceptionHandlerResponseDTO(
    val resource: String?,
    val error: String,
    val status: Int,
    val message: String?,
    val details: Collection<*>?,
    val path: String
) {
    val timestamp = Date()
}
