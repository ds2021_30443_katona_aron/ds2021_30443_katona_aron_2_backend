package com.katonaaron.energy.exposition.handlers

import com.katonaaron.energy.domain.DomainConstraintValidationException
import com.katonaaron.energy.exposition.handlers.exceptions.model.CustomException
import com.katonaaron.energy.exposition.handlers.exceptions.model.ExceptionHandlerResponseDTO
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import org.springframework.validation.ObjectError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import javax.validation.ConstraintViolation
import javax.validation.ConstraintViolationException

@ControllerAdvice
class RestExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [ConstraintViolationException::class])
    fun handleConstraintViolationException(e: ConstraintViolationException, request: WebRequest): ResponseEntity<Any> {
        val status: HttpStatus = HttpStatus.CONFLICT
        val details: Set<ConstraintViolation<*>> = e.constraintViolations
        val errorInformation = ExceptionHandlerResponseDTO(
            e.message,
            status.reasonPhrase,
            status.value(),
            e.message,
            details,
            request.getDescription(false)
        )
        return handleExceptionInternal(
            e,
            errorInformation,
            HttpHeaders(),
            status,
            request
        )
    }

    @ExceptionHandler(value = [DomainConstraintValidationException::class])
    fun handleDomainConstraintValidationException(
        e: DomainConstraintValidationException,
        request: WebRequest
    ): ResponseEntity<Any> {
        val status: HttpStatus = HttpStatus.CONFLICT
        val errorInformation = ExceptionHandlerResponseDTO(
            e.message,
            status.reasonPhrase,
            status.value(),
            e.message,
            e.details,
            request.getDescription(false)
        )
        return handleExceptionInternal(
            e,
            errorInformation,
            HttpHeaders(),
            status,
            request
        )
    }

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        val errs: List<ObjectError> = ex.bindingResult.allErrors
        val details: MutableList<String> = ArrayList()
        for (err in errs) {
            val fieldName = (err as FieldError).field
            val errorMessage = err.getDefaultMessage()
            details.add("$fieldName:$errorMessage")
        }
        val errorInformation = ExceptionHandlerResponseDTO(
            ex.parameter.parameterName,
            status.reasonPhrase,
            status.value(),
            MethodArgumentNotValidException::class.java.simpleName,
            details,
            request.getDescription(false)
        )
        return handleExceptionInternal(
            ex,
            errorInformation,
            HttpHeaders(),
            status,
            request
        )
    }

    @ExceptionHandler(value = [CustomException::class])
    protected fun handleCustomExceptions(
        ex: CustomException,
        request: WebRequest
    ): ResponseEntity<Any> {
        val errorInformation = ExceptionHandlerResponseDTO(
            ex.resource,
            ex.status.reasonPhrase,
            ex.status.value(),
            ex.message ?: "null",
            ex.validationErrors,
            request.getDescription(false)
        )
        return handleExceptionInternal(
            ex,
            errorInformation,
            HttpHeaders(),
            ex.status,
            request
        )
    }
}
