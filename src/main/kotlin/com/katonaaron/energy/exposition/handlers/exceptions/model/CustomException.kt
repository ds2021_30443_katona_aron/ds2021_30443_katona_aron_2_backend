package com.katonaaron.energy.exposition.handlers.exceptions.model

import org.springframework.http.HttpStatus

open class CustomException(
    message: String?,
    val status: HttpStatus,
    val resource: String?,
    val validationErrors: List<String>?
) : RuntimeException(message)
