package com.katonaaron.energy.application.devices

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices

@DDD.ApplicationService
@ApplicationService
class DeleteDevice(
    private val devices: Devices,
) {

    fun deleteDevice(id: DeviceId) {
        val device = devices.findById(id) ?: return

        if (device.hasSensor()) {
            device.removeSensor()
        }

        if (device.hasOwner()) {
            device.removeOwner()
        }

        devices.delete(device)
    }
}
