package com.katonaaron.energy.application.devices.dto

import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.energy.EnergyUnit
import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "Device")
data class DeviceDto(
    val id: String,
    val description: String,
    val address: String,
    val maxEnergyConsumption: Double,
    val baselineEnergyConsumption: Double,
    val currentEnergyConsumption: Double?,
    val currentPowerPeak: Double?
)

fun Device.toDto(): DeviceDto = DeviceDto(
    id = id.value,
    address = address,
    description = description,
    maxEnergyConsumption = maxEnergyConsumption.amount(EnergyUnit.kWh),
    baselineEnergyConsumption = baselineEnergyConsumption.amount(EnergyUnit.kWh),
    currentEnergyConsumption = currentEnergyConsumption?.value?.amount(EnergyUnit.kWh),
    currentPowerPeak = powerPeak?.amount(EnergyUnit.kWh),
)
