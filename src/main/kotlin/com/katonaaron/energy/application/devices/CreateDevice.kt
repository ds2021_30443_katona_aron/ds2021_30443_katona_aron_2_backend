package com.katonaaron.energy.application.devices

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.devices.dto.CreateDeviceDto
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.application.devices.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit

@DDD.ApplicationService
@ApplicationService
class CreateDevice(
    private val devices: Devices
) {
    fun createDevice(dto: CreateDeviceDto): DeviceDto {
        val device = Device(
            id = devices.nextIdentity(),
            address = dto.address,
            description = dto.description,
            maxEnergyConsumption = Energy(dto.maxEnergyConsumption, EnergyUnit.kWh),
            baselineEnergyConsumption = Energy(dto.baselineEnergyConsumption, EnergyUnit.kWh),
        )
        return devices.save(device).toDto()
    }
}
