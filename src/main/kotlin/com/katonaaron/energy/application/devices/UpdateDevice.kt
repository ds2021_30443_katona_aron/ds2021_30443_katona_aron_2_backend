package com.katonaaron.energy.application.devices

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.application.devices.dto.UpdateDeviceDto
import com.katonaaron.energy.application.devices.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class UpdateDevice(
    private val devices: Devices
) {

    fun updateDevice(id: DeviceId, dto: UpdateDeviceDto): DeviceDto {
        val device = devices.findById(id)
            ?: throw ResourceNotFoundException(Device::class.simpleName + " with id: " + id)

        dto.address?.let { device.address = it }
        dto.description?.let { device.description = it }
        dto.maxEnergyConsumption?.let { device.maxEnergyConsumption = Energy(it, EnergyUnit.kWh) }
        dto.baselineEnergyConsumption?.let { device.baselineEnergyConsumption = Energy(it, EnergyUnit.kWh) }

        return devices.save(device).toDto()
    }
}
