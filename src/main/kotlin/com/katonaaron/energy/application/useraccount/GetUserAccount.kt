package com.katonaaron.energy.application.useraccount

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import com.katonaaron.energy.domain.useraccount.UserAccount

@DDD.ApplicationService
@ApplicationService
class GetUserAccount(
    private val sensors: Sensors,
) {
    fun getAccountOfSensor(sensorId: SensorId): UserAccount? {
        return sensors.findById(sensorId)?.device?.owner?.account
    }
}
