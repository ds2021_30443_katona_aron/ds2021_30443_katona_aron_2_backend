package com.katonaaron.energy.application.useraccount

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.useraccount.UserAccount
import com.katonaaron.energy.domain.useraccount.UserAccounts
import com.katonaaron.energy.domain.useraccount.UserRole
import org.springframework.security.crypto.password.PasswordEncoder

@DDD.ApplicationService
@ApplicationService
class CreateUserAccount(
    private val userAccounts: UserAccounts,
    private val passwordEncoder: PasswordEncoder
) {
    fun createUserAccount(username: String, password: String, role: UserRole): UserAccount {
        val account = UserAccount(
            username = username,
            password = passwordEncoder.encode(password),
            role = role,
            id = userAccounts.nextIdentity()
        )
        return userAccounts.save(account)
    }
}
