package com.katonaaron.energy.application.clients.dto

import java.time.LocalDate
import javax.validation.constraints.Past
import javax.validation.constraints.Size

data class UpdateClientDto(
    @field:Size(min = 3, message = "The name must be at least 3 characters long")
    val name: String?,
    @field:Past
    val birthDate: LocalDate?,
    val address: String?
)
