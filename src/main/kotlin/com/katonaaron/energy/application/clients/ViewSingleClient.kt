package com.katonaaron.energy.application.clients

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.clients.dto.ClientDto
import com.katonaaron.energy.application.clients.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class ViewSingleClient(
    private val clients: Clients
) {
    fun viewSingleClient(id: ClientId): ClientDto =
        clients.findById(id)?.toDto()
            ?: throw ResourceNotFoundException(Client::class.simpleName + " with id: " + id)
}
