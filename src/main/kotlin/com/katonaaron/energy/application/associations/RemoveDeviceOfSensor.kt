package com.katonaaron.energy.application.associations

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class RemoveDeviceOfSensor(

    private val sensors: Sensors,
) {
    fun removeDeviceOfSensor(sensorId: SensorId) {
        val sensor = sensors.findById(sensorId)
            ?: throw ResourceNotFoundException("${Sensor::class.simpleName} with id $sensorId")

        if (!sensor.isAttachedToDevice()) {
            return
        }

        sensor.detachFromDevice()
        sensors.save(sensor)
    }
}
