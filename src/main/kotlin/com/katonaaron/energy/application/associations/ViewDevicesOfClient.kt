package com.katonaaron.energy.application.associations

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.application.devices.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class ViewDevicesOfClient(
    private val clients: Clients
) {
    fun viewDevicesOfClient(clientId: ClientId): Collection<DeviceDto> {
        val client = clients.findById(clientId)
            ?: throw ResourceNotFoundException("${Client::class.simpleName} with id $clientId")
        return client.devices.map { it.toDto() }
    }
}
