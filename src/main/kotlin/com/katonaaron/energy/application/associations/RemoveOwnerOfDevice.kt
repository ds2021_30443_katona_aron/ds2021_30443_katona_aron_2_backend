package com.katonaaron.energy.application.associations

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class RemoveOwnerOfDevice(
    private val devices: Devices
) {

    fun removeOwnerOfDevice(deviceId: DeviceId) {
        val device = devices.findById(deviceId)
            ?: throw ResourceNotFoundException("${Device::class.simpleName} with id $deviceId")

        if (!device.hasOwner()) {
            return
        }

        device.removeOwner()
        devices.save(device)
    }
}
