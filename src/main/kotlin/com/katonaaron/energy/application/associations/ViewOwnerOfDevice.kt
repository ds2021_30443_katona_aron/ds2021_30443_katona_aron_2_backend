package com.katonaaron.energy.application.associations

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.clients.dto.ClientDto
import com.katonaaron.energy.application.clients.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class ViewOwnerOfDevice(
    private val devices: Devices
) {
    fun viewOwnerOfDevice(deviceId: DeviceId): ClientDto? {
        val device = devices.findById(deviceId)
            ?: throw ResourceNotFoundException("${Device::class.simpleName} with id $deviceId")
        return device.owner?.toDto()
    }
}
