package com.katonaaron.energy.application.associations

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.application.devices.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class ViewDeviceOfSensor(
    private val sensors: Sensors
) {
    fun viewDeviceOfSensor(sensorId: SensorId): DeviceDto? {
        val sensor = sensors.findById(sensorId)
            ?: throw ResourceNotFoundException("${Sensor::class.simpleName} with id $sensorId")
        return sensor.device?.toDto()
    }
}
