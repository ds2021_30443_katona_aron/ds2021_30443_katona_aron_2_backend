package com.katonaaron.energy.application.sensors

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.sensors.dto.SensorDto
import com.katonaaron.energy.application.sensors.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.Sensors

@DDD.ApplicationService
@ApplicationService
class ViewAllSensors(
    private val sensors: Sensors
) {
    fun viewAllSensors(): Collection<SensorDto> = sensors.findAll().map(Sensor::toDto)
}
