package com.katonaaron.energy.application.sensors

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.sensors.dto.SensorDto
import com.katonaaron.energy.application.sensors.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class ViewSingleSensor(
    private val sensors: Sensors
) {
    fun viewSingleSensor(id: SensorId): SensorDto = sensors.findById(id)?.toDto()
        ?: throw ResourceNotFoundException(Sensor::class.simpleName + " with id: " + id)
}
