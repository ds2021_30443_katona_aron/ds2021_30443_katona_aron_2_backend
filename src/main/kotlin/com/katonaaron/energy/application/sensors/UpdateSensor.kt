package com.katonaaron.energy.application.sensors

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.sensors.dto.SensorDto
import com.katonaaron.energy.application.sensors.dto.UpdateSensorDto
import com.katonaaron.energy.application.sensors.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class UpdateSensor(
    private val sensors: Sensors
) {

    fun updateSensor(id: SensorId, dto: UpdateSensorDto): SensorDto {
        val sensor = sensors.findById(id)
            ?: throw ResourceNotFoundException(Sensor::class.simpleName + " with id: " + id)

        dto.description?.let { sensor.description = it }
        dto.maxValue?.let { sensor.maxValue = Energy(it, EnergyUnit.kWh) }

        return sensors.save(sensor).toDto()
    }
}
