package com.katonaaron.energy.application.consumption.dto

import com.katonaaron.energy.domain.device.EnergyConsumption
import com.katonaaron.energy.domain.energy.EnergyUnit
import io.swagger.v3.oas.annotations.media.Schema
import java.time.ZoneId
import java.time.format.DateTimeFormatter

private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.systemDefault())

@Schema(name = "EnergyConsumption")
data class EnergyConsumptionDto(
    val timestamp: String, val consumption: Double
)

fun EnergyConsumption.toDto(): EnergyConsumptionDto =
    EnergyConsumptionDto(
        timestamp = formatter.format(timestamp),
        consumption = value.amount(EnergyUnit.kWh)
    )
