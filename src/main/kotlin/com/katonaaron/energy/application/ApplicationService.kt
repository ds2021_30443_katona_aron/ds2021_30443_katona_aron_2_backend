package com.katonaaron.energy.application

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
annotation class ApplicationService
