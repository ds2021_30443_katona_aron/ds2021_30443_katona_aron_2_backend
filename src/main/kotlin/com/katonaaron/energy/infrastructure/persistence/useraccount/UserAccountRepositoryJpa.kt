package com.katonaaron.energy.infrastructure.persistence.useraccount

import com.katonaaron.energy.domain.useraccount.UserAccount
import com.katonaaron.energy.domain.useraccount.UserAccountId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserAccountRepositoryJpa : JpaRepository<UserAccount, UserAccountId> {

    fun findByUsername(username: String): UserAccount?
}
