package com.katonaaron.energy.infrastructure.persistence.client

import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ClientRepositoryJpa : JpaRepository<Client, ClientId>
