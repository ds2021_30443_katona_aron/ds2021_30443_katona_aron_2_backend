package com.katonaaron.energy.infrastructure.persistence.device

import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DeviceRepositoryJpa : JpaRepository<Device, DeviceId>
