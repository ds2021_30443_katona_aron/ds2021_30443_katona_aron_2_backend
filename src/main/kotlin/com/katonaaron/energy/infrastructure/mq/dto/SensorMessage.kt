package com.katonaaron.energy.infrastructure.mq.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant

data class SensorMessage(
    @JsonProperty("timestamp") val timestamp: Instant,
    @JsonProperty("sensor_id") val sensor_id: String,
    @JsonProperty("measurement_value") val measurement_value: Double
)
