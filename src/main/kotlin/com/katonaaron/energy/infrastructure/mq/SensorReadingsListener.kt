package com.katonaaron.energy.infrastructure.mq

import com.katonaaron.energy.application.consumption.RecordEnergyConsumption
import com.katonaaron.energy.domain.DomainConstraintValidationException
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.infrastructure.mq.dto.SensorMessage
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitHandler
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component

@Component
@RabbitListener(queues = ["\${com.katonaaron.energy.queue-name}"])
class SensorReadingsListener(
    private val recordEnergyConsumption: RecordEnergyConsumption
) {

    @RabbitHandler
    fun receive(message: SensorMessage) {
        logger.debug("Received message: $message")
        try {
            recordEnergyConsumption.recordEnergyConsumption(
                SensorId(message.sensor_id),
                message.timestamp,
                message.measurement_value
            )
        } catch (e: DomainConstraintValidationException) {
            logger.error(e.toString())
        }
    }

    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val logger = LoggerFactory.getLogger(javaClass.enclosingClass)
    }
}
