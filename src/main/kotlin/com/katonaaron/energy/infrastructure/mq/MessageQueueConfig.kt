package com.katonaaron.energy.infrastructure.mq

import com.fasterxml.jackson.databind.ObjectMapper
import com.katonaaron.energy.infrastructure.mq.dto.SensorMessage
import org.springframework.amqp.core.Queue
import org.springframework.amqp.support.converter.DefaultClassMapper
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration

class MessageQueueConfig {
    @Value("\${com.katonaaron.energy.queue-name}")
    private lateinit var queueName: String

    @Bean
    fun sensorQueue(): Queue {
        return Queue(queueName)
    }

    @Bean
    fun jsonConverter(objectMapper: ObjectMapper): MessageConverter {
        val classMapper = DefaultClassMapper().apply {
            setIdClassMapping(
                mapOf(
                    "SensorMessage" to SensorMessage::class.java
                )
            )
        }
        return Jackson2JsonMessageConverter(objectMapper).apply {
            setClassMapper(classMapper)
        }
    }
}
