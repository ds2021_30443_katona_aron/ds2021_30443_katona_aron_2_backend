package com.katonaaron.energy.infrastructure.security.dto

import com.katonaaron.energy.util.NoArg
import io.swagger.v3.oas.annotations.media.Schema

@NoArg
@Schema
data class LoginDto(
    val username: String,
    val password: String
)
