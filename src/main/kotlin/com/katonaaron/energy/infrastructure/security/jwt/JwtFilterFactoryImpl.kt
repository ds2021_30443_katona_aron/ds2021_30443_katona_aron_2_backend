package com.katonaaron.energy.infrastructure.security.jwt

import com.katonaaron.energy.infrastructure.security.FilterFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Component
import javax.servlet.Filter

@Component
class JwtFilterFactoryImpl(
    private val userDetailsService: UserDetailsService
) : FilterFactory {
    @Value("\${jwt.expiration}")
    private var jwtExpiration: Long = 0

    @Value("\${jwt.key}")
    private lateinit var jwtKey: String

    @Value("\${jwt.issuer}")
    private lateinit var jwtIssuer: String

    @Value("\${jwt.type}")
    private lateinit var jwtType: String

    @Value("\${jwt.audience}")
    private lateinit var jwtAudience: String

    override fun createAuthenticationFilter(authenticationManager: AuthenticationManager): Filter {
        return JwtAuthenticationFilter(
            authenticationManager,
            jwtAudience,
            jwtIssuer,
            jwtKey,
            jwtType,
            jwtExpiration
        )
    }

    override fun createAuthorizationFilter(authenticationManager: AuthenticationManager): Filter {
        return JwtAuthorizationFilter(
            authenticationManager,
            jwtKey,
            jwtIssuer,
            jwtType,
            jwtAudience,
            userDetailsService
        )
    }
}
