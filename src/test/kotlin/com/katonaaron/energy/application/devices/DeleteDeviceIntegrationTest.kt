package com.katonaaron.energy.application.devices

import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class DeleteDeviceIntegrationTest {

    @Autowired
    private lateinit var sensors: Sensors

    @Autowired
    private lateinit var devices: Devices

    @Autowired
    private lateinit var deleteDevice: DeleteDevice

    private val sensorId = SensorId("1")
    private val deviceId = DeviceId("2")

    @BeforeEach
    fun setUp() {
        val sensor = Sensor(
            sensorId,
            "Description",
            Energy(1, EnergyUnit.kWh)
        )
        val device = Device(
            deviceId,
            "Description",
            "address",
            Energy(3, EnergyUnit.kWh),
            Energy(2, EnergyUnit.kWh)
        )
        sensor.attachToDevice(device)
        device.attachSensor(sensor)
        sensors.save(sensor)
        devices.save(device)
    }

    @AfterEach
    fun tearDown() {
    }

    @Test
    fun `when delete device _ it should delete the device`() {
        deleteDevice.deleteDevice(deviceId)
        assertFalse(devices.existsById(deviceId))
    }

    @Test
    fun `when delete device _ it should detach the sensor`() {
        deleteDevice.deleteDevice(deviceId)
        val sensor = sensors.findById(sensorId)
        assertThat(sensor).isNotNull
        assertThat(sensor!!.isAttachedToDevice()).isFalse
    }
}
