package com.katonaaron.energy.application.sensors

import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@SpringBootTest
@ExtendWith(SpringExtension::class)
internal class DeleteSensorIntegrationTest {

    @Autowired
    private lateinit var sensors: Sensors

    @Autowired
    private lateinit var devices: Devices

    @Autowired
    private lateinit var deleteSensor: DeleteSensor

    private val sensorId = SensorId("1")
    private val deviceId = DeviceId("2")

    @BeforeEach
    fun setUp() {
        val sensor = Sensor(
            sensorId,
            "Description",
            Energy(1, EnergyUnit.kWh)
        )
        val device = Device(
            deviceId,
            "Description",
            "address",
            Energy(3, EnergyUnit.kWh),
            Energy(2, EnergyUnit.kWh)
        )
        sensor.attachToDevice(device)
        device.attachSensor(sensor)
        sensors.save(sensor)
        devices.save(device)
    }

    @AfterEach
    fun tearDown() {
    }

    @Test
    fun `when delete sensor it should delete the sensor`() {
        deleteSensor.deleteSensor(sensorId)
        assertFalse(sensors.existsById(sensorId))
    }

    @Test
    fun `when delete sensor it should detach the sensor`() {
        deleteSensor.deleteSensor(sensorId)
        assertFalse(devices.findById(deviceId)!!.hasSensor(), "device has sensors")
    }
}
